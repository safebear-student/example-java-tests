package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by jon on 26/04/2017.
 */
public class WelcomePage {
    WebDriver driver;

    LoginPage loginPage;

    public Boolean checkCorrectPage(WebDriver driver){
        this.driver = driver;
        return driver.getTitle().startsWith("Welcome");
    }

    public Boolean clickOnLogin() {
        loginPage = new LoginPage();
        PageFactory.initElements(driver,this);
        loginLink1.click();
        return loginPage.checkCorrectPage(driver);
    }

    @FindBy(linkText = "Login")
    WebElement loginLink1;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginLink2;

}
