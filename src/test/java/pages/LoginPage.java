package pages;

import org.openqa.selenium.WebDriver;

/**
 * Created by jon on 26/04/2017.
 */
public class LoginPage {
    WebDriver driver;

    public boolean checkCorrectPage(WebDriver driver) {
        this.driver = driver;
        return driver.getCurrentUrl().contains("login");
    }
}
