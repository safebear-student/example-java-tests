package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;

/**
 * Created by jon on 25/04/2017.
 */
public class Utils {
    WebDriver driver;

    String url;

    public Boolean navigateToWebsite(WebDriver driver) {
        this.driver = driver;
        url = System.getProperty("test.host");;
        driver.get(url);
        return driver.getTitle().startsWith("Welcome");
    }

}
