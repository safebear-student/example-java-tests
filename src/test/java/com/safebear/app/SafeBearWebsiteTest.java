package com.safebear.app;

import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.LoginPage;
import pages.WelcomePage;

import static org.junit.Assert.assertTrue;

/**
 * Created by jon on 24/04/2017.
 */
public class SafeBearWebsiteTest {

    WebDriver driver;
    LoginPage loginPage;
    WelcomePage welcomePage;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/jon/selenium/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.setBinary("/Applications/Google Chrome 2.app/Contents/MacOS/Google Chrome");
        driver = new ChromeDriver(options);
        Utils utility;
        utility = new Utils();
        assertTrue(utility.navigateToWebsite(driver));
        loginPage = new LoginPage();
        welcomePage = new WelcomePage();
    }

    @Test
    public void testWelcomePage() {
        assertTrue(welcomePage.checkCorrectPage(driver));
        assertTrue(welcomePage.clickOnLogin());
    }
    @After
    public void testDown() {
        driver.close();
    }
}
